<?php
declare(strict_types=1);

namespace App\Models\Entities;

use App\Log;
use DateTime;
use DateTimeZone;
use NumberFormatter;

/**
 * The main User class This object will contain the user information and 1 or more locations or transactions
 * depending on the usecase. When used with a transaction it will only have a single transaction and a single location.
 */
class User
{
    public string $id;
    public string $title;
    public string $first_name;
    public string $last_name;
    public string $email;
    public string $dob;
    public string $registered_date;
    public ?string $phone;
    public ?string $cell;
    public string $gender;
    public string $picture_large;
    public string $picture_medium;
    public string $picture_thumbnail;
    public ?array $locations;
    public ?array $transactions;

    public function __construct(array $data = [])
    {
        if (!empty($data)) {
            foreach ($data as $key => $value) {
                if (preg_match('/^location\.(.+?)$/', $key, $matches)) {
                    $this->location[$matches[1]] = $value;
                    continue;
                }
                $this->$key = (string)$value;
            }
        }
        if(isset($this->json_locations)) {
            $this->locations = json_decode($this->json_locations,true);
            if(!json_last_error()) {
                unset($this->json_locations);
            } else {
                (new Log())->simpleLog('./logs/userConstruct.log', "Json Error", ['jsonstring'=>$this->json_locations]);
            }
        }
        if(isset($this->json_transactions)) {
            $this->transactions = json_decode($this->json_transactions,true);
            if(!json_last_error()) {
                unset($this->json_transactions);
            } else {
                (new Log())->simpleLog('./logs/userConstruct.log', "Json Error", ['jsonstring'=>$this->json_transactions]);
            }
        }
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return User
     */
    public function setId(string $id): User
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return User
     */
    public function setTitle(string $title): User
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->first_name;
    }

    /**
     * @param string $first_name
     * @return User
     */
    public function setFirstName(string $first_name): User
    {
        $this->first_name = $first_name;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->last_name;
    }

    /**
     * @param string $last_name
     * @return User
     */
    public function setLastName(string $last_name): User
    {
        $this->last_name = $last_name;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return User
     */
    public function setEmail(string $email): User
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string|null $phone
     * @return User
     */
    public function setPhone(?string $phone): User
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCell(): ?string
    {
        return $this->cell;
    }

    /**
     * @param string|null $cell
     * @return User
     */
    public function setCell(?string $cell): User
    {
        $this->cell = $cell;
        return $this;
    }

    /**
     * @return string
     */
    public function getGender(): string
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     * @return User
     */
    public function setGender(string $gender): User
    {
        $this->gender = $gender;
        return $this;
    }

    /**
     * @return string
     */
    public function getPictureLarge(): string
    {
        return $this->picture_large;
    }

    /**
     * @param string $picture_large
     * @return User
     */
    public function setPictureLarge(string $picture_large): User
    {
        $this->picture_large = $picture_large;
        return $this;
    }

    /**
     * @return string
     */
    public function getPictureMedium(): string
    {
        return $this->picture_medium;
    }

    /**
     * @param string $picture_medium
     * @return User
     */
    public function setPictureMedium(string $picture_medium): User
    {
        $this->picture_medium = $picture_medium;
        return $this;
    }

    /**
     * @return string
     */
    public function getPictureThumbnail(): string
    {
        return $this->picture_thumbnail;
    }

    /**
     * @param string $picture_thumbnail
     * @return User
     */
    public function setPictureThumbnail(string $picture_thumbnail): User
    {
        $this->picture_thumbnail = $picture_thumbnail;
        return $this;
    }

    /**
     * @return array|mixed
     */
    public function getLocations(): mixed
    {
        return $this->locations;
    }

    /**
     * @param array $locations
     * @return User
     */
    public function setLocations(array $locations): User
    {
        $this->locations = $locations;
        return $this;
    }

    /**
     * @return array
     */
    public function getTransactions(): mixed
    {
        return $this->transactions;
    }

    /**
     * @param array $transactions
     * @return User
     */
    public function setTransactions(array $transactions): User
    {
        $this->transactions = $transactions;
        return $this;
    }


    /**
     * @return string
     */
    public function getDob(): string
    {
        $offset = $this->getTimezoneOffset($this->locations[0]['timezone_offset']);
        $tz = new DateTimeZone($offset);
        return (new DateTime($this->dob, $tz))->format('F d, Y');
    }

    /**
     * @param bool $includeTitle
     * @return string
     */
    public function getFullName(bool $includeTitle = true): string
    {
        return $includeTitle ? $this->title . " " . $this->first_name . " " . $this->last_name : $this->first_name . " " . $this->last_name;
    }

    /**
     * Returns a formatted registered data
     * @return string
     */
    public function getRegisteredDate(): string
    {
        $regD = new DateTime(
            $this->registered_date,
            new DateTimeZone($this->getTimezoneOffset($this->locations[0]['timezone_offset'])));
        return $regD->format('F d, Y');
    }

    /**
     * Calculates the RegisteredAge
     * @return int
     */
    public function getRegisteredAge(): int
    {
        return $this->getAgeOfDate($this->registered_date) ?? 0;
    }

    /**
     * Calculates the Age of the user.
     * @return bool|int
     */
    public function getAge(): bool|int
    {
        return $this->getAgeOfDate($this->getDob());
    }

    /**
     * @param string $date
     * @return bool|int
     */
    private function getAgeOfDate(string $date): bool|int
    {
        $dateObj = new DateTime($date);
        return $dateObj->diff(new DateTime())->y;
    }

    /**
     * Returns an array of labels based on the user property names.
     * @return array
     */
    public function getColumnLabels():array
    {
        $keys = array_keys($this->toArray());
        $labels = array_map(function($key){
            return strtoupper(str_replace('_',' ',$key));
        }, $keys);
        return array_combine($keys,$labels);
    }

    public function getTransactionData(string $amount, string $payment_method){

       return  [
            'users_id' => $this->getId(),
            'users_locations_id' => $this->getLocations()[0]['id'],
            'amount' => number_format((float)$amount, 2, '.', ''),
            'status' => 'pending',
            'payment_method' => $payment_method
        ];
    }
    /**
     * @return array
     */
    public function toArray(): array
    {
        $props = get_object_vars($this);
        if(isset($props['transactions'])) {
            if (isset($this->locations)) {
                $locations = $this->getLocations();
            }
            foreach ($props['transactions'] as $transactionIdx => $transaction) {
                $props['transactions'][$transactionIdx]['amount'] = (new NumberFormatter(
                    'en-US', NumberFormatter::CURRENCY
                ))->formatCurrency($transaction['amount'], 'USD');
                $props['transactions'][$transactionIdx]['created_date'] = (new DateTime(
                    (string)$transaction['created_date'], new DateTimeZone('UTC')
                ))->format('m/d/Y');
                if (isset($this->locations)) {
                    $transactionLocation = array_map(function($k) use ($locations) {
                        return $locations[$k];
                    }, array_keys(array_column($locations, 'id'), $transaction['users_locations_id']));
                    $props['transactions'][$transactionIdx]['location'] = $transactionLocation[0];
                }

            }
        }
        //add ages and location array
        $props['full_name'] = $this->getFullName();
        $props['age'] = $this->getAge();
        $props['dob'] = $this->getDob();
        $props['registered_age'] = $this->getRegisteredAge();
        return $props;
    }

    /**
     * Checks the timezone offset if it = 0:00 it will use UTC.
     * @param string $offset
     * @return string
     */
    public function getTimezoneOffset(string $offset): string
    {
        return $offset === '0:00' ? 'UTC' : $offset;
    }


}