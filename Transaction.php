<?php
declare(strict_types=1);

namespace App\Models\Entities;

use App\Models\Entities\User;
use App\Log;
use DateTime;
use DateTimeZone;
use NumberFormatter;

/**
 * This is a Transaction object that will contain the transaction data as well as the user and location for that transaction.
 */
class Transaction
{
    public string $id;
    public string $users_id;
    public string $users_locations_id;
    public string $amount;
    public string $status;
    public string $payment_method;
    public string $created_date;
    private ?array $location;
    private ?User $user;

    public function __construct(array $data = [])
    {
        //Loads the basic properties. If json_user and json_u is included the location object will be complete.
        //NOTE; not tested This is filled via PDO::fetch under normal circumstances.
        if (!empty($data)) {
            foreach ($data as $key => $value) {
                $this->$key = (string)$value;
            }
        }
        if (isset($this->json_locations)) {
            $this->location = json_decode($this->json_locations, true);
            if (isset($this->user)) {
                $this->user->setLocations([$this->location]);
            }
            if (!json_last_error()) {
                unset($this->json_locations);
            } else {
                (new Log())->simpleLog(
                    './logs/transactionConstruct.log', "Json Error", ['jsonstring' => $this->json_locations]
                );
            }
        }
        if (isset($this->json_u)) {

            $this->user = new User(json_decode($this->json_u, true));
            if (isset($this->location)) {
                $this->user->setLocations([$this->location]);
            }
            if (!json_last_error()) {
                unset($this->json_u);
            } else {
                (new Log())->simpleLog('./logs/transactionConstruct.log', "Json Error", ['jsonstring' => $this->json_u]
                );
            }
        }
        //Since PDO doesn't call the constructor When we do make sure we format the date and amount.
        if (isset($this->amount)) {
            $this->setAmount($this->amount);
        }
        if (isset($this->created_date)) {
            $this->setCreatedDate($this->created_date);
        }
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return Transaction
     */
    public function setId(string $id): Transaction
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getUsersId(): string
    {
        return $this->users_id;
    }

    /**
     * @param string $users_id
     * @return Transaction
     */
    public function setUsersId(string $users_id): Transaction
    {
        $this->users_id = $users_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getUsersLocationsId(): string
    {
        return $this->users_locations_id;
    }

    /**
     * @param string $users_locations_id
     * @return Transaction
     */
    public function setUsersLocationsId(string $users_locations_id): Transaction
    {
        $this->users_locations_id = $users_locations_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getAmount(): string
    {
        return $this->amount;
    }

    /**
     * @param string $amount
     * @return Transaction
     */
    public function setAmount(string $amount): Transaction
    {
        $formatted = (new NumberFormatter('en-US', NumberFormatter::CURRENCY))
            ->formatCurrency((float)$amount, 'USD');
        $this->amount = $formatted;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return Transaction
     */
    public function setStatus(string $status): Transaction
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getPaymentMethod(): string
    {
        return $this->payment_method;
    }

    /**
     * @param string $payment_method
     * @return Transaction
     */
    public function setPaymentMethod(string $payment_method): Transaction
    {
        $this->payment_method = $payment_method;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedDate(): string
    {
        return $this->created_date;
    }

    /**
     * Preformats the created_date/transaction date.
     * @param string $created_date
     * @return Transaction
     */
    public function setCreatedDate(string $created_date): Transaction
    {
        $formatted = (new DateTime($created_date, new DateTimeZone('UTC')))->format('m/d/Y');
        if (!$formatted) {
            (new Log())->simpleLog(
                './logs/transaction.log',
                'Created Date "' . $created_date . '" could not be formatted for Transaction: ' . $this->getId()
            );
            $formatted = $created_date;
        }
        $this->created_date = $formatted;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getLocation(): ?array
    {
        return $this->location;
    }

    /**
     * @param array|null $location
     * @return Transaction
     */
    public function setLocation(?array $location): Transaction
    {
        $this->location = $location;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getUser(): ?array
    {
        return $this->user->toArray();
    }

    /**
     * @param array|null $user
     * @return Transaction
     */
    public function setUser(?array $user): Transaction
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return get_object_vars($this);
    }
}