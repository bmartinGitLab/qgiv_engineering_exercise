<?php
declare(strict_types=1);
namespace App\Data;

use PDO;
use PDOStatement;
use App\Log;

/**
 * Handles the PDO connection and select insert queries. PRior to execustion fill in the config.php info.
 * Ideally The credentials would be in an environment var.
 */
class Connector
{
    protected int $fetchMode;
    private int $defaultFetchMode;
    private object $db;
    private bool $dbConfigOk;
    private PDO $connection;

    /**
     * Constructor takes an optional defaultFetchMode param that defaults to FETCH_BOTH
     * @param int $defaultFetchMode
     */
    public function __construct(int $defaultFetchMode = PDO::FETCH_BOTH)
    {
        $this->db = include 'config.php';
        $this->dbConfigOk = true;
        if(empty($this->db->user) || empty($this->db->password)) {
            $this->dbConfigOk = false;
        }
        $this->defaultFetchMode = $defaultFetchMode;
    }

    /**
     * During construct the config user and password is check if not set this will be false
     * @return bool
     */
    public function isDbConfigSet():bool
    {
        return $this->dbConfigOk;
    }

    /**
     * Connects to the database if not connected.
     * @return bool
     */
    public function connect():bool
    {
        if(!isset($this->connection)) {
            $dns = "{$this->db->driver}:dbname={$this->db->schema};host={$this->db->host};charset={$this->db->charset}";
            try {
                $this->connection = new PDO(
                    $dns,
                    $this->db->user,
                    $this->db->password,
                    [
                        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                        PDO::ATTR_DEFAULT_FETCH_MODE,$this->defaultFetchMode
                    ]
                );
            } catch (\PDOException $exception) {
                (new \App\Log)->logThrowable('./logs/database.log', $exception);
                return false;
            }
        }
        return true;
    }

    public function setFetchMode(int $mode):self
    {
        $this->fetchMode = $mode;
        return $this;
    }
    /**
     * Selects data from the database returning the record as
     * @param string $sql
     * @param array $params
     * @param string $className
     * @return bool|mixed
     */
    public function select(string $sql, array $params = [], string $className = ''):mixed
    {
        $stmt = $this->prepare($sql,$params,$className);
        return $stmt !== false?$stmt->fetch():false;
    }

    /**
     * Selects all records returning them in an array.
     * @param string $sql
     * @param array $params
     * @param string $className
     * @return bool|array
     */
    public function selectAll(string $sql, array $params = [], string $className = ''): bool| array
    {
        $stmt = $this->prepare($sql, $params, $className);
        return $stmt !== false?$stmt->fetchAll():false;
    }

    /**
     * @return bool
     */
    public function beginTransaction():bool
    {
        if (!$this->connect()) {
            return false;
        }
        return $this->connection->beginTransaction();
    }

    /**
     * @return bool
     */
    public function commit():bool
    {
        return $this->connection->commit();
    }

    /**
     * @return bool
     */
    public function rollBack():bool
    {
        return $this->connection->rollBack();
    }

    /**
     * @param string $sql
     * @param array $params
     * @return bool|int
     */
    public function insert(string $sql, array $params):bool|int
    {

        $stmt = $this->prepare($sql,$params);
        if ($stmt !== false) {
            $lastInsertId = (int)$this->connection->lastInsertId();
            return $lastInsertId;
        }
        return false;
    }

    /**
     * Prepares and Executes the statement and sets the fetch mode if a class is passed in.
     * @param string $sql
     * @param array $params
     * @param string $className
     * @return bool|PDOStatement
     */
    public function prepare(string $sql, array $params, string $className = ''):? PDOStatement
    {   //Establish the connection or return the existing connection if it exists.
        if (!$this->connect()) {
            return false;
        }
        $stmt = $this->connection->prepare($sql);
        $stmt->execute($params);
        if (!empty($className)) {
            $stmt->setFetchMode($this->fetchMode ?? PDO::FETCH_CLASS, $className);
        }
        return $stmt;
    }
}

