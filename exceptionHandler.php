<?php
/**
 * Logs exceptions not handled.
 * @param Throwable $exception
 * @return void
 */
function exception_handler(Throwable $exception) {
    (new App\Log())->logThrowable('./logs/uncaughtExceptions.log', $exception);
}

set_exception_handler('exception_handler');