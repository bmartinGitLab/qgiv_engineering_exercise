-- MariaDB dump 10.19  Distrib 10.4.22-MariaDB, for osx10.17 (x86_64)
--
-- Host: localhost    Database: qgiv_engineering_excercise
-- ------------------------------------------------------
-- Server version	10.4.22-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `import_log`
--

DROP TABLE IF EXISTS `import_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `import_log` (
  `seed` varchar(25) NOT NULL,
  `results` int(11) NOT NULL,
  `page` int(11) NOT NULL,
  `version` varchar(10) NOT NULL,
  `total_added` int(11) NOT NULL DEFAULT 0,
  `last_load_date` timestamp NOT NULL DEFAULT current_timestamp(),
  UNIQUE KEY `import_log_seed_uindex` (`seed`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Logs the import of users using the payload info. ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `import_log`
--

LOCK TABLES `import_log` WRITE;
/*!40000 ALTER TABLE `import_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `import_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment_methods`
--

DROP TABLE IF EXISTS `payment_methods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_methods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_methods`
--

LOCK TABLES `payment_methods` WRITE;
/*!40000 ALTER TABLE `payment_methods` DISABLE KEYS */;
INSERT INTO `payment_methods` VALUES (1,'Visa'),(2,'Mastercard'),(3,'Discover'),(4,'American Express'),(5,'eCheck'),(6,'Apple Pay'),(7,'Google Pay'),(8,'Samsung Pay');
/*!40000 ALTER TABLE `payment_methods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transactions`
--

DROP TABLE IF EXISTS `transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL,
  `users_locations_id` int(11) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `status` varchar(25) NOT NULL,
  `payment_method` varchar(256) NOT NULL,
  `created_date` timestamp(3) NOT NULL DEFAULT current_timestamp(3),
  PRIMARY KEY (`id`),
  KEY `transactions_users_id_fk` (`users_id`),
  KEY `transactions_users_locations_id_fk` (`users_locations_id`),
  CONSTRAINT `transactions_users_id_fk` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`),
  CONSTRAINT `transactions_users_locations_id_fk` FOREIGN KEY (`users_locations_id`) REFERENCES `users_locations` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='This is the transactions table.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transactions`
--

LOCK TABLES `transactions` WRITE;
/*!40000 ALTER TABLE `transactions` DISABLE KEYS */;
/*!40000 ALTER TABLE `transactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(256) DEFAULT NULL,
  `first_name` varchar(256) NOT NULL,
  `last_name` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `dob` datetime(3) NOT NULL,
  `registered_date` datetime(3) NOT NULL,
  `phone` varchar(256) DEFAULT NULL,
  `cell` varchar(256) DEFAULT NULL,
  `gender` varchar(256) DEFAULT NULL,
  `picture_large` varchar(256) NOT NULL,
  `picture_medium` varchar(256) NOT NULL,
  `picture_thumbnail` varchar(256) NOT NULL,
  `modified_date` timestamp(3) NOT NULL DEFAULT current_timestamp(3),
  `created_date` timestamp(3) NOT NULL DEFAULT '0000-00-00 00:00:00.000',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_first_name_last_name_gender_uindex` (`email`,`first_name`,`last_name`,`gender`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Users Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_locations`
--

DROP TABLE IF EXISTS `users_locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL,
  `street_number` varchar(10) NOT NULL,
  `street_name` varchar(256) NOT NULL,
  `city` varchar(256) NOT NULL,
  `state` varchar(256) NOT NULL,
  `country` varchar(256) NOT NULL,
  `postcode` varchar(20) NOT NULL,
  `latitude` varchar(20) DEFAULT NULL,
  `longitude` varchar(20) DEFAULT NULL,
  `timezone_offset` varchar(6) DEFAULT NULL,
  `timezone_description` varchar(256) DEFAULT NULL,
  `created_date` datetime(3) NOT NULL DEFAULT current_timestamp(3),
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_locations_street_coordinates_index` (`users_id`,`street_number`,`street_name`,`postcode`,`latitude`,`longitude`),
  CONSTRAINT `users_locations_users_id_fk` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='The location for the users.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_locations`
--

LOCK TABLES `users_locations` WRITE;
/*!40000 ALTER TABLE `users_locations` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_pictures`
--

DROP TABLE IF EXISTS `users_pictures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_pictures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL,
  `large` varchar(256) NOT NULL,
  `medium` varchar(256) NOT NULL,
  `thumbnail` varchar(256) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `users_pictures_users_id_fk` (`users_id`),
  CONSTRAINT `users_pictures_users_id_fk` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='The Users pictures to display';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_pictures`
--

LOCK TABLES `users_pictures` WRITE;
/*!40000 ALTER TABLE `users_pictures` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_pictures` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-12-20  2:07:28
