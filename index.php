<?php
require_once './bootstrap.php';

use App\Controller;

require_once 'exceptionHandler.php';
//router
session_start();
$requestPath = $_SERVER['REQUEST_URI'];
$requestMethod = $_SERVER['REQUEST_METHOD'];
$data = [];
$controller = new Controller();

if (preg_match('/^\/$/', $requestPath)) {
    if ($requestMethod === 'GET') {
        $controller->index();
    } else {
        $controller->errorResponseHeader(404, "The path requested was not found.");
    }
} elseif (preg_match('/^\/transactions\/?$/', $requestPath)) {
    if ($requestMethod === 'GET') {
        $controller->showTransactions();

    } else {
        $controller->errorResponseHeader(404, "The path requested was not found.");
    }
} elseif (preg_match('/^\/transactions\/(\d+)\/?$/', $requestPath, $id)) {

    if ($requestMethod === 'GET') {
        $controller->showTransaction((int)$id[1]);
    } else {
        $controller->errorResponseHeader(404, "The path requested was not found.");
    }
} elseif (preg_match('/^\/users\/?$/', $requestPath)) {
    if ($requestMethod === 'GET') {
        $controller->showUsers();
    } else {
        $controller->errorResponseHeader(404, "The path requested was not found.");
    }
} elseif (preg_match('/^\/users\/(\d+)\/?$/', $requestPath, $id)) {
    if ($requestMethod === 'GET') {
        $controller->showUser((int)$id[1]);
    } elseif ($requestMethod === 'POST') {
        if ($_POST['token'] === $_SESSION['csrf-token']) {
            $controller->saveUserTransaction((int)$id[1]);
        } else {
            $controller->errorResponseHeader(403, "The path requested is forbidden.");
        }
    } else {
        $controller->errorResponseHeader(404, "The path requested was not found.");
    }
} else {
    $controller->errorResponseHeader(403, "The path requested is forbidden.");
}

