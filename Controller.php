<?php
declare(strict_types=1);

namespace App;

use App\Data\Connector;
use App\Models\Entities\User;

/**
 * This is the main controller pulling data from the Model and rendering the views.
 *
 */
class Controller
{
    /**
     * This first calls load then calls the showUsers to list all users.
     * @return void
     */
    public function index():void
    {
        //check db config, if user and pass not set prompt to config the app.
        $connection = new Connector();
        if ($connection->isDbConfigSet()) {
            $this->load();
            $this->showTransactions();
           return;
        }
        // Unnecessary for this but thought I might get to a JS frontend :(
        $message = 'There was an error connecting to the database. Please check the config.';
        if (preg_match('/application\/json/', $_SERVER['HTTP_ACCEPT'])) {
            $this->errorResponseHeader(500, $message, 'json');
        } else {
            $this->errorResponseHeader(500, $message);
        }
    }

    /**
     * Loads a single user with all of the transactions made and all locations for the user.
     * @param int $id
     * @return void
     */
    public function showUser(int $id, string $message = ''):void
    {
        $model = new Model();
        $userEntity = $model->getUserBy($id);
        $userAsObj = (object)$userEntity->toArray();
        $data = ['title'=>' | User ','user'=>$userAsObj];
        if (!empty($message)){
            $data['notification']=['message'=>$message,'type'=>'notice'];
        }
        //Get payment_methods
        $pms = $model->getPaymentMethods();
        $data['paymentMethods']=$pms;
        $token = md5(uniqid((string)mt_rand(), true));
        $_SESSION['csrf-token'] = $token;
        $data['csrf-token'] = $token;
        $this->renderView('./views/user.php',$data);
    }

    /**
     * Load ALL users from the database.
     * Needs a paginator.
     * @return void
     */
    public function showUsers()
    {
        $model = new Model();
        $data = ['title'=>' | Users ','users'=>$model->getUsers()];
        $this->renderView('./views/users.php', $data );
    }

    /**
     * loads a single transaction with user and location data.
     * @param int $id
     * @return void
     */
    public function showTransaction(int $id)
    {
        $model = new Model();
        $transaction = $model->getTransactionById($id);
        $data = ['title'=>' | Transaction '.$transaction->getUser()['first_name'], 'transaction'=>$transaction];
        $this->renderView('./views/transaction.php', $data);
    }

    /**
     * Retieves the Transactions from the database and renders the view.
     *  Needs a paginator.
     * @return void
     */
    public function showTransactions()
    {
        $model = new Model();
        $data = ['title'=>' | Transactions', 'transactions'=>$model->getTransactions()];
        $this->renderView('./views/transactions.php', $data);
    }

    public function saveUserTransaction($id):void
    {
        $model = new Model();
        $user = $model->getUserBy($id);
        $amount = $_POST['amount'];
        $payment_method = $_POST['payment_method'];
        if( !is_numeric($amount)){
            $this->showUser($id,"Please enter a valid amount.");
        }
        $transactionData =$user->getTransactionData($amount,$payment_method);
        $transactionid = $model->createTransaction($transactionData);
        if(!$transactionid) {
            $message = 'An attempt to save the following transaction failed';
            (new Log())->simpleLog('./logs/failedTransaction.log',
                                   $message.': ',
                                   $transactionData);
            $this->errorResponseHeader(500, $message);
        }
        unset($_SESSION['csrf-token']);
        $this->showuser($id);

    }

    /**
     * Load the users if it hasn't run before. We could make this look at the last load date
     * to determine if it should load more.
     * @return void
     */
    public function load()
    {
        $connection = new Connector();
        $hasLoaded = $connection->select('select * from import_log order by last_load_date desc');
        if ($hasLoaded === false) {
            $jsonData = $this->getUsersFromApi();

            //import into the db
            $importData = json_decode($jsonData, true);
            if (json_last_error()) {
                $this->errorResponseHeader(json_last_error(),json_last_error_msg());
                return;
            }
            //Structure = [Results:[[],[]],info:[]]
            //use the users class
            $userModel = new Model();
            $preCnt = $userModel->getUsersCount();
            foreach ($importData['results'] as $idx => $user) {
                //re-map the user record to match the db.
                if ($userModel->importUserArray($user) === false) {
                    (new Log())->simpleLog('../logs/load.log'," 007 User Not imported ",$user);
                }
            }
            $cnt = $userModel->getUsersCount();
            $info = $importData['info'];
            $recordsPulled = (int)$info['results'];
            $added = $cnt - $preCnt;
            $info['total_added'] = $added;
            $userModel->saveImportInfo($info);
        }
    }
    //Section: Model API
    /**
     * @return string
     */
    public function getUsersFromApi(): string
    {
        // Load Data
        $dataUrl = 'https://randomuser.me/api/?results=500&nat=us&exc=login,id,nat';
        $curler = curl_init();
        if ($curler === false) {
            $message = curl_error($curler);
            if (preg_match('/application\/json/', $_SERVER['HTTP_ACCEPT'])) {
                $this->errorResponseHeader(500, $message, 'json');
            } else {
                $this->errorResponseHeader(500, $message);

            }
            return "";
        }
        curl_setopt($curler, CURLOPT_URL, $dataUrl);
        curl_setopt($curler, CURLOPT_RETURNTRANSFER, true);
        $headers = [
            'Accept: text/json,application/json',
            'Accept-Encoding: gzip, deflate', //request gzip.
            'Cache-Control: no-cache',
            'Content-Type: application/json; charset=utf-8'
        ];
        curl_setopt($curler, CURLOPT_HTTPHEADER, $headers);
        $zippedData = curl_exec($curler);
        //before we move on close the handle to free up system resources.
        curl_close($curler);
        if ($zippedData === false) {
            return "";
        }
        $jsonData = zlib_decode($zippedData);
        if ($jsonData === false) {
            return "";
        }
        return $jsonData;
    }

    //Section: Response Renderers.
    /**
     * Returns an error for type and exits.
     * @param int $code
     * @param string $message
     * @param string $type
     * @return void
     */
    public function errorResponseHeader(int $code, string $message, string $type = 'html'): void
    {
        $codes = [
            400 => 'Bad Request',
            403 => 'Forbidden',
            500 => 'Internal Server Error',
            404 => 'Not Found',
            401 => 'Unauthorized'
        ];
        $data = [
            "title" => $code . ' ' . $codes[$code],
            "status" => "Error",
            "message" => $message
        ];

        switch ($type) {
            case 'json':
                header('Content-Type: application/json;');
                $this->renderJson(json_encode($data), $code);
                break;
            default:
                $this->renderView('./errors/error.php',$data,'html',$code);
        }
        exit;
    }

    /**
     * renders an html page injecting $data array.
     * @param string $template
     * @param array $data
     * @param string $responceType
     * @param int $code
     * @return void
     */
    private function renderView(string $template, array $data=[], string $responceType ='html', int $code = 200):void
    {
        $codes = [
            200 => 'OK',
            400 => 'Bad Request',
            403 => 'Forbidden',
            500 => 'Internal Server Error',
            404 => 'Not Found',
            401 => 'Unauthorized'
        ];
        http_response_code($code);
        header('Status: ' . $code);
        header('HTTP/1.1 ' . $code. ' ' . $codes[$code]);
        if(!empty($template) && !file_exists($template)) {
            header('Content-Type: text/html; charset=UTF-8');
            echo '';
            return;
        }

        switch ($responceType) {
            case 'json':
                header('Content-Type: application/json;');
                echo json_encode($data);
                break;
            default:
                header('Content-Type: text/html; charset=UTF-8');
        }

       require $template;
    }

    /**
     * Extra: Future dev to return json to JS
     * @param string $json
     * @param int $code
     * @return void
     */
    private function renderJson(string $json, int $code=200):void
    {
        $codes = [
            200 => 'OK',
            400 => 'Bad Request',
            403 => 'Forbidden',
            500 => 'Internal Server Error',
            404 => 'Not Found',
            401 => 'Unauthorized'
        ];
        http_response_code($code);
        header('Status: ' . $code);
        header('HTTP/1.1 ' . $code. ' ' . $codes[$code]);
        header('Content-Type: application/json;');
        echo $json;
    }
}