<?php
declare(strict_types=1);

namespace App;

use App\Models\Entities\User;
use App\Models\Entities\Transaction;
use App\Data\Connector;
use DateTime;
use DateTimeZone;
use PDO;

/**
 * This is the main model class for this exercise.
 * I combined what would normally be in multiple models
 * t0 make this a little more compact.
 * This model is used to pull the data from the database and return in as casses or arrays depending on what is needed.
 */
class Model
{
    // "Y-m-d\\TH:i:s.vp"; //The original datetime format of the payload.
    private const DATETIME_FORMAT = "Y-m-d H:i:s.v"; //This will keep the microseconds for accuracy
    private $connection;


    /**
     * @return int
     */
    public function getUsersCount(): int
    {
        $this->connection = new Connector();
        $sql = "select count(id) as users_count from users";
        $cnt = $this->connection->select($sql);
        return (int)$cnt['users_count'];
    }

    /**
     * @return int
     */
    public function getUsersLocationsCount(): int
    {
        $this->connection = new Connector();
        $sql = "select count(id) as locations_count from users_locations";
        $cnt = $this->connection->select($sql);
        return (int)$cnt['locations_count'];
    }

    /**
     * In preperation for pagination. More params would be required.
     * @return int
     */
    public function getTransactionsCount(): int
    {
        $this->connection = new Connector();
        $sql = "select count(id) as transactions_count from transactions";
        $cnt = $this->connection->select($sql);
        return (int)$cnt['transactions_count'];
    }

    /**
     * Returns a single transaction by id.
     * @param int $id
     * @return Transaction
     */
    public function getTransactionById(int $id): Transaction
    {
        $connection = new Connector();
        //These column values are used over and over should refactor.
        $locationColumns = ['id','users_id','street_number','street_name','city','state','country',
        'postcode','latitude','longitude','timezone_offset','timezone_description'];
        $transactionColumns = ['`transactions`.`id`','`transactions`.`users_id`',
        '`transactions`.`users_locations_id`','`transactions`. `amount`','`transactions`.`status`',
        '`transactions`.`payment_method`', '`transactions`.`created_date`'];

        $usersColumns = ['id','title','first_name','last_name','email','dob','registered_date','phone',
        'cell','gender','picture_large','picture_medium','picture_thumbnail' ];

        $sql = 'SELECT '
            . implode(', ',$transactionColumns) . ', '
            . $this->formatJsonObjectQuery($locationColumns,'locations') . ' AS json_locations, '
            . $this->formatJsonObjectQuery($usersColumns, 'u') . ' AS json_u'
            . ' FROM transactions '
            . ' LEFT JOIN users `u` on `transactions`.`users_id` = `u`.`id` '
            . ' LEFT JOIN users_locations `locations` on `transactions`.`users_locations_id` = `locations`.`id` '
            . ' WHERE '
            . ' `transactions`.`id`= :id'
            . ' GROUP BY (`u`.`id`) ORDER BY `transactions`.`id` DESC';

        return $connection->select($sql, [':id' => $id], Transaction::class);
    }

    /**
     * Gets all of the transactions Again PAginations would be nice :(
     * @return array
     */
    public function getTransactions():array
    {
        $connection = new Connector();
        //These column values are used over and over may as well keep them here.
        $locationColumns = ['id','users_id','street_number','street_name','city','state','country',
            'postcode','latitude','longitude','timezone_offset','timezone_description'];
        $transactionColumns = ['`transactions`.`id`','`transactions`.`users_id`',
            '`transactions`.`users_locations_id`','`transactions`. `amount`','`transactions`.`status`',
            '`transactions`.`payment_method`', '`transactions`.`created_date`'];

        $usersColumns = ['id','title','first_name','last_name','email','dob','registered_date','phone',
            'cell','gender','picture_large','picture_medium','picture_thumbnail' ];


        $sql = 'SELECT '
            . implode(', ',$transactionColumns) . ', '
            . $this->formatJsonObjectQuery($locationColumns,'locations') . ' AS json_locations, '
            . $this->formatJsonObjectQuery($usersColumns, 'u') . ' AS json_u '
            . ' FROM transactions '
            . ' LEFT JOIN users `u` on `transactions`.`users_id` = `u`.`id` '
            . ' LEFT JOIN users_locations `locations` on `transactions`.`users_locations_id` = `locations`.`id` '
            . ' GROUP BY (`u`.`id`) ORDER BY `transactions`.`id` DESC';
        return $connection->selectAll($sql, [], Transaction::class);
    }

    /**
     * Saves the paylod info to a log table to use to schedule imports or prevent the auto load from running every time.
     * @param array $info
     * @return bool
     */
    public function saveImportInfo(array $info): bool
    {
        $columns = array_keys($info);
        [$paramKeys, $params] = $this->getParamsAndKeys($columns, $info);
        $sql = "INSERT INTO import_log (" . implode(', ', $columns) . ") VALUES (" . implode(', ', $paramKeys) . ")";
        $this->connection = new Connector();
        $insertId = $this->connection->insert($sql, $params);
        if ($insertId === false) {
            (new Log())->simpleLog('./logs/database.log', 'Failed to save import info.', $info);
            return false;
        }
        return true;
    }

    /**
     * imports the user data as retrieved from the API.
     ****  NOTE: This function has way too much logic in it and should be refactored
     ****        to make the process more understandable and testable.
     * @param array $user
     * @return bool
     */
    public function importUserArray(array $user): bool
    {
        //Establish the connection to the db.
        $this->connection = new Connector();
        if (!$this->connection->beginTransaction()) {
            (new Log())->logThrowable('./logs/database.log', new \Exception("Transactions failed to begin!"));
            return false;
        }
        //Remember to use the timezone offset to get as accurately as possible.
        $dobSafe = new DateTime(
            $user['dob']['date'], new DateTimeZone($this->getValidTimezone($user['location']['timezone']['offset']))
        );

        //Now use the datetime_format of: "Y-m-d H:i:s.v" to store the datetime(3) in the db.
        //Same for registered_date
        $regOnSafe = new DateTime(
            $user['registered']['date'],
            new DateTimeZone($this->getValidTimezone($user['location']['timezone']['offset']))
        );
        $userData = [
            'title' => $user['name']['title'],
            'first_name' => $user['name']['first'],
            'last_name' => $user['name']['last'],
            'email' => $user['email'],
            'dob' => $dobSafe->format(self::DATETIME_FORMAT),
            'registered_date' => $regOnSafe->format(self::DATETIME_FORMAT),
            'phone' => $user['phone'],
            'cell' => $user['cell'],
            'gender' => $user['gender'],
            'picture_large' => $user['picture']['large'],
            'picture_medium' => $user['picture']['medium'],
            'picture_thumbnail' => $user['picture']['thumbnail'],
        ];
        //Create the column names and params array
        $columns = array_keys($userData);
        $indexedColumns = ['email', 'first_name', 'last_name', 'gender'];
        [$paramKeys, $params, $updateKeyValues] = $this->getParamsAndKeys($columns, $userData, $indexedColumns, true);

        // Create the sql statement to pass to prepare.
        $sql = "INSERT INTO users (" . implode(', ', $columns) . ", created_date) VALUES (" . implode(
                ', ', $paramKeys
            ) . ", :created_date) ON DUPLICATE KEY UPDATE " . implode(', ', $updateKeyValues);
        // Prepare the created date, this ensures we are always looking at UTC Date times for records we create.
        // We can always convert them to the users timezone later using the offset, if need be, this seems just
        // more of a sanity check on when the user was created in this system.
        $UTCTime = new DateTime(); //Use for created_date fields we generate.
        $UTCTime->setTimezone(new DateTimeZone('UTC'));
        // Inject the create_date into the params.
        $params[':created_date'] = $UTCTime->format(self::DATETIME_FORMAT);
        $userInsertId = $this->connection->insert($sql, $params);
        if ($userInsertId === false) {
            $this->connection->rollBack();
            (new Log())->logThrowable(
                './logs/database.log', new \Exception(
                                         'User Exists: SQL: ' . str_replace('\n', '', $sql) . ' User: ' . json_encode(
                                             $user
                                         )
                                     )
            );
            return false;
        }

        $locationData = [
            'users_id' => $userInsertId,
            'street_number' => $user['location']['street']['number'],
            'street_name' => $user['location']['street']['name'],
            'city' => $user['location']['city'],
            'state' => $user['location']['state'],
            'country' => $user['location']['country'],
            'postcode' => $user['location']['postcode'],
            'latitude' => $user['location']['coordinates']['latitude'],
            'longitude' => $user['location']['coordinates']['longitude'],
            'timezone_offset' => $user['location']['timezone']['offset'],
            'timezone_description' => $user['location']['timezone']['description'],
        ];
        $columns = array_keys($locationData);
        $indexedColumns = ['users_id', 'street_number', 'street_name', 'postcode', 'latitude', 'longitude'];
        [$paramKeys, $params, $updateKeyValues] = $this->getParamsAndKeys(
            $columns, $locationData, $indexedColumns, true
        );

        $sql = "INSERT INTO users_locations "
            . "(" . implode(', ', $columns) . ")"
            . " VALUES "
            . "(" . implode(', ', $paramKeys) . ")"
            . " ON DUPLICATE KEY UPDATE " . implode(', ', $updateKeyValues);

        // Call insert to actually insert the data.
        $locationInsertId = $this->connection->insert($sql, $params);
        if ($locationInsertId === false) {
            $this->connection->rollBack();
            (new Log())->logThrowable(
                './logs/database.log', new \Exception(
                                         'User Exists: SQL: ' . str_replace('\n', '', $sql) . ' User: ' . json_encode(
                                             $user
                                         )
                                     )
            );
            return false;
        }
        //Call the create transaction method.
        if ($this->createTransaction($this->generateTransactionData($userInsertId, $locationInsertId)) === false) {
            //rollback and log
            $this->connection->rollBack();
            (new Log())->logThrowable(
                './logs/database.log', new \Exception(
                                         'Transaction Exists: SQL: ' . str_replace(
                                             '\n', '', $sql
                                         ) . ' User: ' . json_encode($user)
                                     )
            );
            return false;
        }
        return $this->connection->commit();
    }


    /**
     * Returns the user identified by ID.
     * @param int $id
     * @return User
     */
    public function getUserBy(int $id): User
    {
        $this->connection = new Connector(PDO::FETCH_ASSOC);
        $columns = [
            '`u`.`id`',
            '`u`.`title`',
            '`u`.`first_name`',
            '`u`.`last_name`',
            '`u`.`email`',
            '`u`.`dob`',
            '`u`.`registered_date`',
            '`u`.`phone`',
            '`u`.`cell`',
            '`u`.`gender`',
            '`u`.`picture_large`',
            '`u`.`picture_medium`',
            '`u`.`picture_thumbnail`'
        ];
        $locationColumns = [
            'id',
            'users_id',
            'street_number',
            'street_name',
            'city',
            'state',
            'country',
            'postcode',
            'latitude',
            'longitude',
            'timezone_offset',
            'timezone_description'
        ];
        $transactionColumns = [
            'id',
            'users_id',
            'users_locations_id',
            'amount',
            'status',
            'payment_method',
            'created_date'
        ];

        // Get the user record with the last location as determined by the id.
        $sql = 'SELECT '
            . implode(', ', $columns)
            . ', ' . $this->formatJsonGroupColumn($locationColumns, 'locations')
            . ', ' . $this->formatJsonGroupColumn(
                $transactionColumns, 'transactions', ['order_by' => 'created_date', 'direction' => 'DESC']
            )
            . ' FROM '
            . '`users` AS `u`'
            . ' LEFT JOIN `transactions` ON `transactions`.`users_id` = `u`.`id`'
            . ' LEFT JOIN `users_locations` AS `locations` ON `transactions`.`users_locations_id` = `locations`.`id`'
            . ' WHERE `'
            . 'u`.`id` = :id'
            . ' GROUP BY (`u`.`id`)';


        $user = $this->connection->select($sql, [':id' => $id], User::class);
        return $user;
    }

    /**
     * Returns ALL of the users Started to add in pagination, didn't get to it. :(
     * @param int $page
     * @param int $max
     * @return array
     */
    public function getUsers(int $page = 1, int $max = 0): array
    {
        $connection = new Connector();
        //This query groups all locations a user may have into a json object
        // The user entity on construct will convert to an array to consume.
        $locationColumns = [
            'id',
            'users_id',
            'street_number',
            'street_name',
            'city',
            'state',
            'country',
            'postcode',
            'latitude',
            'longitude',
            'timezone_offset',
            'timezone_description'
        ];
        $sql = <<< EOMEGAQUERY
SELECT u.* ,
       {$this->formatJsonGroupColumn($locationColumns, 'locations')}
FROM users AS u
    LEFT JOIN users_locations locations ON u.id = locations.users_id
GROUP BY (locations.users_id);
EOMEGAQUERY;
        $allUsers = $connection->selectAll($sql, [], User::class);
        return $allUsers;
    }

    //Section: Utility methods

    /**
     * Simplified the querys when adding a json object top the result set.
     * @param array $columns
     * @param string $table_alias
     * @return string
     */
    public function formatJsonObjectQuery(array $columns, string $table_alias): string
    {
        $keyValues = array_map(function($k) use ($table_alias) {
            return "'$k', `$table_alias`.`$k`";
        }, $columns);

        return "JSON_OBJECT(" . implode(', ', $keyValues) . ")";
    }

    /**
     * Simplified creating the queries when including the json array in the result set.
     * @param array $columns
     * @param string $table_alias
     * @param array $sortGroupOrder
     * @return string
     */
    public function formatJsonGroupColumn(
        array  $columns,
        string $table_alias,
        array  $sortGroupOrder = ['order_by' => 'id', 'direction' => 'DESC']
    ): string {
        $result = "CONCAT('[', GROUP_CONCAT( DISTINCT "
            . $this->formatJsonObjectQuery($columns, $table_alias)
            . " ORDER BY "
            . "`" . $table_alias . "`.`" . $sortGroupOrder['order_by'] . "` "
            . $sortGroupOrder['direction']
            . " SEPARATOR '," . '\n' . "') "
            . ", ']') AS json_" . $table_alias;

        return $result;
    }

    /**
     * Creates a transaction. In a production environment it may be best to store a copy of the user info
     * and location with the transaction or link to a copy of in another table incase the user has the ability to
     * alter the info.
     * @param array $transactionData
     * @return bool|int
     */
    public function createTransaction(array $transactionData): bool|int
    {
        $columns = array_keys($transactionData);
        [$paramKeys, $params] = $this->getParamsAndKeys($columns, $transactionData);
        $sql = 'INSERT INTO transactions '
            . '(' . implode(', ', $columns) . ')'
            . ' VALUES '
            . '(' . implode(', ', $paramKeys) . ')';
        return $this->connection->insert($sql, $params);
    }

    /**
     * @param array $columns
     * @param array $insertData
     * @return array
     */
    public function getParamsAndKeys(
        array $columns,
        array $insertData,
        array $exludeIndexColumns = [],
        bool  $includeUpdateKeys = false
    ): array {
        $paramKeys = array_map(function($k) {
            return ':' . $k;
        }, $columns);
        $params = array_combine($paramKeys, array_values($insertData));
        $result = [$paramKeys, $params];
        if ($includeUpdateKeys) {
            $updateKeyValues = array_map(function($k) use ($exludeIndexColumns) {
                if (!in_array($k, $exludeIndexColumns)) {
                    return '`' . $k . '` = ' . ':' . $k;
                }
                return false;
            }, $columns);

            $result[] = array_filter($updateKeyValues, function($element) {
                return $element;
            });
        }

        return $result;
    }

    /**
     * This data would normally come from an actual transaction.
     * It is randomly generated in this exercise
     * @param $user_id
     * @param $locationId
     * @return array
     */
    private function generateTransactionData($user_id, $locationId): array
    {
        $statuses = ['pending', 'approved', 'declined', 'closed'];
        $availablePaymentMethods = $this->getPaymentMethods();
        $randomAmount = (double)rand(0, 10000) . '.' . str_pad((string)rand(0, 99), 2, "0");
        return [
            'users_id' => $user_id,
            'users_locations_id' => $locationId,
            'amount' => number_format((float)$randomAmount, 2, '.', ''),
            'status' => $statuses[rand(0, count($statuses) - 1)],
            'payment_method' => $availablePaymentMethods[rand(0, count($availablePaymentMethods) - 1)]
        ];
    }

    /**
     * DateTimeZone can't use a 0:00 offset, so we return UTC.
     * @param $offset
     * @return mixed|string
     */
    public function getValidTimezone($offset): mixed
    {
        return $offset !== '0:00' ? $offset : 'UTC';
    }

    /**
     * @return array|bool
     */
    public function getPaymentMethods(): array|bool
    {
        $connection = new Connector();
        $connection->setFetchMode(PDO::FETCH_NUM);
        $availablePaymentMethods = $connection->selectAll('select `name` from payment_methods');
        $result = array_map(function($val){
            return $val[0];
        },$availablePaymentMethods);
        return $result;
    }
}