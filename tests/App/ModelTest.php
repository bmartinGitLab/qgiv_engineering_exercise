<?php

namespace App;

use PHPUnit\Framework\TestCase;
use App\Model;

class ModelTest extends TestCase
{
    protected Model $model;
    protected array $mockData;
    public function setUp(): void
    {
        $this->mockData = json_decode('{"gender": "female", "name": {"title": "Miss","first": "Madison","last": "Gonzalez"},"location": {"street": {  "number": 2,  "name": "Westheimer Rd"},"city": "Saint Paul","state": "West Virginia","country": "United States","postcode": 76130,"coordinates": {"latitude": "-24.2972",  "longitude": "165.7500"},"timezone": {  "offset": "-1:00",  "description": "Azores, Cape Verde Islands"}},"email": "madison.gonzalez@example.com", "dob": {"date": "1969-03-12T23:50:23.959Z","age": 53}, "registered": {"date": "2002-07-08T02:22:22.979Z","age": 20}, "phone": "(835) 870-4860", "cell": "(634) 497-9089", "picture": {"large": "https://randomuser.me/api/portraits/women/50.jpg","medium": "https://randomuser.me/api/portraits/med/women/50.jpg","thumbnail": "https://randomuser.me/api/portraits/thumb/women/50.jpg" }}', true);
        $this->model = new Model();
    }

    public function testGetParamsAndKeys()
    {
        $locationData = [
            'users_id' => 1,
            'street_number' => $this->mockData['location']['street']['number'],
            'street_name' => $this->mockData['location']['street']['name'],
            'city' => $this->mockData['location']['city'],
            'state' => $this->mockData['location']['state'],
            'country' => $this->mockData['location']['country'],
            'postcode' => $this->mockData['location']['postcode'],
            'latitude' => $this->mockData['location']['coordinates']['latitude'],
            'longitude' => $this->mockData['location']['coordinates']['longitude'],
            'timezone_offset' => $this->mockData['location']['timezone']['offset'],
            'timezone_description' => $this->mockData['location']['timezone']['description'],
        ];
        $columns = array_keys($locationData);
        $indexedColumns = ['users_id', 'street_number', 'street_name', 'postcode', 'latitude', 'longitude'];
        [$paramKeys, $params , $updateKeyValues] = $this->model->getParamsAndKeys($columns, $locationData, $indexedColumns, true );
        $this->assertNotEmpty($paramKeys);
        $this->assertNotEmpty($params);
        $this->assertNotEmpty($updateKeyValues);
        $this->assertEquals(5,count($updateKeyValues));
        $this->assertArrayNotHasKey('users_id',array_flip($updateKeyValues));
    }

    public function testFormatJsonGroupColumn()
    {
        $expected = "CONCAT('[', GROUP_CONCAT( JSON_OBJECT('id', `users_locations`.`id`, 'street_number', `users_locations`.`street_number`, 'street_name', `users_locations`.`street_name`) ORDER BY `users_locations`.`id` DESC SEPARATOR ',".'\n'."') , ']') AS json_users_locations";
        $actual = $this->model->formatJsonGroupColumn(['id','street_number','street_name'],'users_locations');
        $this->assertStringContainsString($expected,$actual);
    }

    public function testFormatJsoneObectQuery()
    {
        $expected = $expected = "JSON_OBJECT('id', `users_locations`.`id`, 'street_number', `users_locations`.`street_number`, 'street_name', `users_locations`.`street_name`)";
        $actual = $this->model->formatJsonObjectQuery(['id','street_number','street_name'],'users_locations');
        $this->assertStringContainsString($expected,$actual,'FormatJsonObjectQuery Returned unexpected result.');
    }
}
