<?php

namespace App\Models\Entities;

use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    protected User $userEntity;
    protected $mockData = '{"id":"100","gender": "female","title": "Miss","first_name": "Madison","last_name": "Gonzalez","location.id":"500","location.users_id":"100", "location.street_number": 2,"location.street_name": "Westheimer Rd","location.city": "Saint Paul", "location.state": "West Virginia","location.country": "United States","location.postcode": 76130,"location.latitude": "-24.2972","location.longitude": "165.7500","location.timezone_offset": "-1:00","location.timezone_description": "Azores, Cape Verde Islands","email": "madison.gonzalez@example.com","dob": "1969-03-12T23:50:23.959Z", "registered_date": "2002-07-08T02:22:22.979Z", "phone": "(835) 870-4860", "cell": "(634) 497-9089","picture_large": "https://randomuser.me/api/portraits/women/50.jpg", "picture_medium": "https://randomuser.me/api/portraits/med/women/50.jpg", "picture_thumbnail": "https://randomuser.me/api/portraits/thumb/women/50.jpg"}';

    protected function setUp(): void
    {
        parent::setup();
        $userData = json_decode($this->mockData, true);
        $this->userEntity = new User($userData);
    }


    public function testToArray()
    {
        $actual = $this->userEntity->toArray();
        $this->assertNotEmpty($actual);
        $this->assertArrayHasKey('location', $actual);
    }
}
