<?php 
/*
- create a database to hold transaction records and users
- loads in a list of users from JSON: https://randomuser.me/api/?results=500&nat=us&exc=login,id,nat
    - add unique users to a database table
    - as users are loaded, generate a transaction record tied to the user
    - create a User class to work with the data
        - use the class to format birthdate to the following format: January 1, 1990
        - write a method to generate a transaction for the user
            - in addition to storing user details, transactions should have:
                - a unique ID
                - timestamp
                - amount   
                - status
                - payment method (Visa, Mastercard, Discover, American Express, eCheck, or any other new payment method that pops up)
        - anything else you think would be helpful for working with the data

- display a table of transactions with data displayed
- format transactions in the following format: MM/DD/YYYY
- default sorted by transaction ID
- display a table of users with data displayed
- clicking the user ID takes you to a view that displays that users details and associated transactions
- note: logic should be built without utilizing PHP libraries

- bonus: add dynamic table sorting   
- bonus: add pagination
*/

/*
If you need help setting up a development environment:
    Download and run mamp from https://www.mamp.info/en/

    After installation, run the MAMP app (should not need pro)
    On Mac, located at /Applications/MAMP/MAMP
    "Start Servers"

    Your dev stack should be up and running locally. You can access the database through phpMyAdmin at http://localhost:8888/phpMyAdmin/?lang=en

    Php and html should live in /Applications/MAMP/htdocs/
*/
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width" />
    
    <title>Qgiv Engineering Exercise<?= $data['title']??'' ?></title>
    
    <link rel="stylesheet" type="text/css" href="https://secure.qgiv.com/resources/admin/css/application.css" />

    <style type="text/css">
        .container{ max-width: 1200px; margin: 0 auto; }

        .logo-header{ padding: 2em; }
        .logo{ margin: 0 auto; min-height: 80px; }
    </style>
    <link rel="stylesheet" type="text/css" href="../extra.css" />
</head>
<?php $user = $data['user']; ?>
<body>
    <section class="container">
        <div class="logo-header">
            <img class="logo" src="https://secure.qgiv.com/resources/core/images/logo-qgiv.svg" alt="Qgiv logo" />
        </div>
        <article class="xtra-card nav-card">
            <ul>
                <li><a href="/transactions">Transactions</a></li>
                <li><a href="/users">Users</a></li>
            </ul>
        </article>
        <article class="xtra-card user-card">
            <table>
                <tr>
                    <th>Name: </th>
                    <td><?= $user->full_name ?></td><td rowspan="4"><img src="<?= $user->picture_large ?>" alt="<?= $user->full_name ?>"/> </td>

                </tr>
                <tr>
                    <th>Email: </th>
                    <td><?= $user->email ?></td>
                </tr>
                <tr>
                    <th>
                        Phone:
                    </th>
                    <td>
                        <?= $user->phone?? '' ?>
                    </td>
                </tr>
                <tr>
                    <th>Cell: </th>
                    <td><?= $user->cell ?? '' ?></td>
                </tr>
                <tr>
                    <th>Location<?= count($user->locations) >1?'s':'' ?>: </th>
                    <td>
                        <div class="list">
                            <ul>
                        <?php foreach($user->locations as $k=>$location): ?>
                        <li>
                            <p><?= $location['street_number']." ".$location['street_name']."<br />".$location['city'].", ".$location['state']." ".$location['postcode']."<br />".$location['country'] ?></p>
                            <p>Coordinates: <?= $location['latitude'].", ".$location['longitude']."<br /> Timezone: ".$location['timezone_description']." ".$location['timezone_offset'] ?></p>
                        </li>
                        <?php endforeach; ?>
                        </ul>
                        </div>
                    </td>
                    <td>
                        <form action="/users/<?= $user->id ?>" method="POST" >
                            <input type="hidden" name="token" value="<?= $_SESSION['csrf-token'] ?>"/>
                            <fieldset>
                                <p>
                            <label for="amount">Amount</label>
                            <input type="text" name="amount" pattern="^\d+(\.\d{2}){0,1}$" placeholder="100.99" title="Amount should only contain a valid dollar amount."/><br />
                                </p>
                                <p>
                            <label for="payment_method">Method</label>
                            <select name="payment_method">
                                <?php foreach($data['paymentMethods'] as $k=>$name): ?>
                                <option id='method_<?=$k?>' value="<?= $name ?>"><?= $name ?></option>
                                <?php endforeach; ?>
                            </select></p>
                                <button type="submit">Make Transaction</button>
                            </fieldset>

                        </form>
                    </td>
                </tr>
            </table>
        </article>

        <div class="data-table-container">
            <table class="data-table">
                <thead>
                    <tr>
                        <th class="ui-secondary-color">ID</th>
                        <th class="ui-secondary-color">AMOUNT</th>
                        <th class="ui-secondary-color">STATUS</th>
                        <th class="ui-secondary-color">PAYMENT METHOD</th>
                        <th class="ui-secondary-color">LOCATION</th>
                        <th class="ui-secondary-color">DATE</th>
                    </tr>
                </thead>

                <tbody>
                <?php /*
 $id;,$title;,$first_name;,$last_name;,$email;,$dob;,$registered_date;,
 $phone;,$cell;,$gender;,$picture_large;,$picture_medium;,$picture_thumbnail;,
 $locations;
 */ ?>
                    <?php foreach($user->transactions as $transaction): ?>
                    <tr class="data-row">
                        <td><a href="/transactions/<?= $transaction['id'] ?>"><?= $transaction['id'] ?></a></td>
                        <td><?= $transaction['amount'] ?></td>
                        <td><?= $transaction['status'] ?></td>
                        <td><?= $transaction['payment_method'] ?></td>
                        <td><?= $location['street_number']." ".$location['street_name']."<br />".$location['city'].", ".$location['state']." ".$location['postcode']."<br />".$location['country']."<br/> Coordinates: ".$location['latitude'].", ".$location['longitude']."<br /> Timezone: ".$location['timezone_description']." ".$location['timezone_offset']  ?></td>
                        <td>
                            <?= $transaction['created_date']; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </section>
</body>
</html>