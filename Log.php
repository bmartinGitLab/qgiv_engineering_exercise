<?php
declare(strict_types=1);

namespace App;

use Throwable;

/**
 * Simplified logger
 */
class Log
{
    /**
     * Simplified Exception logger.
     * @param $file
     * @param Throwable $e
     * @return void
     */
    public function logThrowable($file, Throwable $e):void
    {
        $logentry = date('Y-m-d H:i:s.v')." ".$e->getCode()." ".$e->getMessage()." ".$e->getTraceAsString()."\n";
        file_put_contents($file,$logentry,FILE_APPEND | LOCK_EX);
    }

    /**
     * Logs arbitrary messages.
     * @param string $file
     * @param string $message
     * @param array $extra
     * @return void
     */
    public function simpleLog(string $file, string $message, array $extra=[]):void
    {
        $logentry = date('Y-m-d H:i:s.v').' '.$message;
        if(!empty($extra)) {
            $logentry .= ' ' . json_encode($extra);
        }
        $logentry .= '\n';
        file_put_contents($file,$logentry,FILE_APPEND | LOCK_EX);
    }
}